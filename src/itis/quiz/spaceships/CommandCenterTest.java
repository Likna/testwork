package itis.quiz.spaceships;

import java.util.ArrayList;

public class CommandCenterTest {

    public CommandCenterTest() {
        first =  new Spaceship("First", 0, 5, 20);
        second =  new Spaceship("Second", 20, 15, 30);
        third =  new Spaceship("Third", 30, 25, 40);
        fourth =  new Spaceship("Fourth", 40, 35, 50);
    }

    private final Spaceship first;
    private final Spaceship third;
    private final Spaceship fourth;
    private final Spaceship second;

    private double testingResult;

    public double getTestingResult() {
        makeAllTests();
        return testingResult;
    }

    void makeAllTests(){
        double point = 0.5;

        if (getShipByName_returnsCorrectShipTest())
            testingResult+=point;
        if (getShipByName_returnsNullTest())
            testingResult+=point;

        if (getAllCivilianShips_returnsCorrectShipTest())
            testingResult+=point;
        if (getAllCivilianShips_returnsNullTest())
            testingResult+=point;

        if (getAllShipsWithEnoughCargoSpace_returnsCorrectShipTest())
            testingResult+=point;
        if (getAllShipsWithEnoughCargoSpace_returnsNullTest())
            testingResult+=point;

        point = 0.4;
        if (getMostPowerfulShip_returnsCorrectShipTest())
            testingResult+=point;
        if (getMostPowerfulShip_returnsFirstCorrectShipTest())
            testingResult+=point;
        if (getMostPowerfulShip_returnsNullTest())
            testingResult+=point;

        if (testingResult>4)
            testingResult=4;
    }

    //getShipByName returning boolean testing
    public boolean getShipByName_returnsCorrectShipTest(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> spaceships = new ArrayList<>();
        spaceships.add(first);
        spaceships.add(second);
        spaceships.add(third);
        spaceships.add(fourth);
        return commandCenter.getShipByName(spaceships, "Third").getName().equals("Third");
    }

    //getShipByName returning null testing
    public boolean getShipByName_returnsNullTest(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> spaceships = new ArrayList<>();
        spaceships.add(first);
        return commandCenter.getShipByName(spaceships, "Third") == null;
    }

    //getAllShipsWithEnoughCargoSpace returning boolean testing
    public boolean getAllShipsWithEnoughCargoSpace_returnsCorrectShipTest(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> spaceships = new ArrayList<>();
        spaceships.add(first);
        spaceships.add(second);
        return (commandCenter.getAllShipsWithEnoughCargoSpace(spaceships,10).contains(second) &&
                !commandCenter.getAllShipsWithEnoughCargoSpace(spaceships,10).contains(first));
    }

    //getAllShipsWithEnoughCargoSpace returning null testing
    public boolean getAllShipsWithEnoughCargoSpace_returnsNullTest(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> spaceships = new ArrayList<>();
        spaceships.add(first);
        spaceships.add(second);
        return commandCenter.getAllShipsWithEnoughCargoSpace(spaceships,100).isEmpty();
    }

    //getAllCivilianShips returning boolean testing
    public boolean getAllCivilianShips_returnsCorrectShipTest(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> spaceships = new ArrayList<>();
        spaceships.add(first);
        spaceships.add(first);
        spaceships.add(second);
        return commandCenter.getAllCivilianShips(spaceships).size() == 2;
    }

    //getAllCivilianShips returning null testing
    public boolean getAllCivilianShips_returnsNullTest(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> spaceships = new ArrayList<>();
        spaceships.add(third);
        spaceships.add(third);
        spaceships.add(second);
        return commandCenter.getAllCivilianShips(spaceships).isEmpty();
    }

    //getMostPowerfulShip returning most
    public boolean getMostPowerfulShip_returnsCorrectShipTest(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> spaceships = new ArrayList<>();
        spaceships.add(first);
        spaceships.add(third);
        spaceships.add(second);
        return commandCenter.getMostPowerfulShip(spaceships).equals(third);
    }

    //getMostPowerfulShip returning most
    public boolean getMostPowerfulShip_returnsFirstCorrectShipTest(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> spaceships = new ArrayList<>();
        spaceships.add(first);
        spaceships.add(third);
        spaceships.add(second);
        spaceships.add(new Spaceship("Fifth", 30, 35, 50));
        return (commandCenter.getMostPowerfulShip(spaceships).equals(third));
    }

    //getMostPowerfulShip returning null
    public boolean getMostPowerfulShip_returnsNullTest(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> spaceships = new ArrayList<>();
        spaceships.add(new Spaceship("",-5,0,0));
        return commandCenter.getMostPowerfulShip(spaceships) == null;
    }
}
