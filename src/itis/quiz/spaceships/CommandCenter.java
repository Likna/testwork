package itis.quiz.spaceships;


import javax.lang.model.type.NullType;
import java.util.ArrayList;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager {

    @Override
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
        int min = -1;
        Spaceship found = null;
        for (Spaceship ship: ships) {
            if(ship.getFirePower()>min) {
                min = ship.getFirePower();
                found = ship;
            }
        }
        return found;
    }

    @Override
    public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {

        for (Spaceship ship: ships) {
            if(ship.getName().equals(name)) {
                return ship;
            }
        }
        return null;
    }

    @Override
    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {
        ArrayList<Spaceship> suitable = new ArrayList<>();

        for (Spaceship ship: ships) {
            if(ship.getCargoSpace()>=cargoSize) {
                suitable.add(ship);
            }
        }
        return suitable;
    }

    @Override
    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
        ArrayList<Spaceship> suitable = new ArrayList<>();

        for (Spaceship ship: ships) {
            if(ship.getFirePower()==0) {
                suitable.add(ship);
            }
        }
        return suitable;
    }
}
