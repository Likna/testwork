package itis.quiz.spaceships;
import org.junit.jupiter.api.*;

import java.util.ArrayList;

public class TestWithLibrary {

    private static Spaceship first =  new Spaceship("First", 0, 5, 20);
    private static Spaceship second =  new Spaceship("Second", 20, 15, 30);
    private static Spaceship third =  new Spaceship("Third", 30, 25, 40);
    private static Spaceship fourth =  new Spaceship("Fourth", 40, 35, 50);

    @Test
    public void getShipByName_returnsCorrectShipTest(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> spaceships = new ArrayList<>();
        spaceships.add(first);
        spaceships.add(second);
        spaceships.add(third);
        spaceships.add(fourth);
        assert commandCenter.getShipByName(spaceships, "Third").getName().equals("Third");
    }

    @Test
    public void getShipByName_returnsNullTest(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> spaceships = new ArrayList<>();
        spaceships.add(first);
        assert commandCenter.getShipByName(spaceships, "Third") == null;
    }

    @Test
    public void getAllShipsWithEnoughCargoSpace_returnsCorrectShipTest(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> spaceships = new ArrayList<>();
        spaceships.add(first);
        spaceships.add(second);
        assert (commandCenter.getAllShipsWithEnoughCargoSpace(spaceships,10).contains(second) &&
                !commandCenter.getAllShipsWithEnoughCargoSpace(spaceships,10).contains(first));
    }

    @Test
    public void getAllShipsWithEnoughCargoSpace_returnsNullTest(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> spaceships = new ArrayList<>();
        spaceships.add(first);
        spaceships.add(second);
        assert commandCenter.getAllShipsWithEnoughCargoSpace(spaceships,100).isEmpty();
    }

    @Test
    public void getAllCivilianShips_returnsCorrectShipTest(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> spaceships = new ArrayList<>();
        spaceships.add(first);
        spaceships.add(first);
        spaceships.add(second);
        assert commandCenter.getAllCivilianShips(spaceships).size() == 2;
    }

    @Test
    public void getAllCivilianShips_returnsNullTest(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> spaceships = new ArrayList<>();
        spaceships.add(third);
        spaceships.add(third);
        spaceships.add(second);
        assert commandCenter.getAllCivilianShips(spaceships).isEmpty();
    }

    @Test
    public void getMostPowerfulShip_returnsCorrectShipTest(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> spaceships = new ArrayList<>();
        spaceships.add(first);
        spaceships.add(third);
        spaceships.add(second);
        assert commandCenter.getMostPowerfulShip(spaceships).equals(third);
    }

    @Test
    public void getMostPowerfulShip_returnsFirstCorrectShipTest(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> spaceships = new ArrayList<>();
        spaceships.add(first);
        spaceships.add(third);
        spaceships.add(second);
        spaceships.add(new Spaceship("Fifth", 30, 35, 50));
        assert (commandCenter.getMostPowerfulShip(spaceships).equals(third));
    }

    @Test
    public void getMostPowerfulShip_returnsNullTest(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> spaceships = new ArrayList<>();
        spaceships.add(new Spaceship("",-5,0,0));
        assert commandCenter.getMostPowerfulShip(spaceships) == null;
    }

}
