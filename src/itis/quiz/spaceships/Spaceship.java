package itis.quiz.spaceships;

public class Spaceship {
	private final String name;

	public String getName() {
		return name;
	}

	//огневая мощь
	private int firePower;

	public int getFirePower() {
		return firePower;
	}

	//размер грузового трюма
	private int cargoSpace;

	public int getCargoSpace() {
		return cargoSpace;
	}

	//прочность
	private int durability;

	public int getDurability() {
		return durability;
	}

	public Spaceship(String name, int firePower, int cargoSpace, int durability) {
		this.name = name;
		this.firePower = firePower;
		this.cargoSpace = cargoSpace;
		this.durability = durability;
	}
}
